<?php

namespace App\Controller;

use ApiPlatform\Core\Serializer\JsonEncoder;
use App\Repository\MosqueRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder as EncoderJsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class MosqueController extends AbstractController
{

    private $serializer;
    private $traits = array(
                "attributes",
                "signs",
                "mosques",
                "weather" );
    private $result = array();
    private $limit = 10;

    function __construct()
    {
        $encoders = [new XmlEncoder(), new EncoderJsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $this->serializer = new Serializer($normalizers, $encoders);
    }

    private function FormResponseArray(array $array, array $check_array, string $tokenId): array
	{
        if(!isset($array['image'])){
            $array['image'] = '';
        }

		if (!is_null($array['image'])) {

			foreach ($array as $key => $value) {
				if (in_array($key, $check_array) && $value != 'NULL') {
					$this->result['traits'][$key] = $value;
				}
			}
			$this->result['traits']['image'] = 'https://img.tronhumanoids.com/img/'.$array['id'].'.png';

			$this->result['tokenId'] = $array['id'];
			$this->result['tokenIdNum'] = $array['id'];
			$this->result['image'] = 'https://img.tronhumanoids.com/img/'.$array['id'].'.png';

		} else {

			$this->result['traits'] = NULL;
			$this->result['tokenId'] = $tokenId;
			$this->result['tokenIdNum'] = $tokenId;
			$this->result['image'] = NULL;
		}

		return $this->result;
	}


        #[Route('/api/search/{id}', name: 'search_mosque', methods:['GET'])]
    public function SearchById(int $id, MosqueRepository $mosqueRepository): JsonResponse
    {
        $query_result = $mosqueRepository->find($id);
        $query_result = $this->serializer->normalize($query_result, 'array');

        if(!empty($query_result)){

            $this->result['data'][] = $this->FormResponseArray($query_result, $this->traits, $query_result['id']);
            $this->result['meta']['pagination']['limit'] = $this->limit;
            $this->result['meta']['pagination']['page'] = 1;
            $this->result['meta']['pagination']['total'] = 1;
            $this->result['meta']['sort']['by'] = 'id';
            $this->result['meta']['sort']['order'] = 'ASC';
            $this->result['isSuccess'] = true;
        }else{
            $this->result['isSuccess'] = false;
        }
        
        return new JsonResponse($this->result);
    }


        #[Route('/api/gallery', name: 'gallery_mosque', methods:['GET'])]
    public function Gallery(Request $request, MosqueRepository $mosqueRepository): JsonResponse
    {
        $query_params = array();
        $q_params = array(
                            "name",
                            "backgroundColor",
                            "rarity",
                            "rarityRank",
                            "attributes",
                            "signs",
                            "mosques",
                            "weather",);
        $offset = 0;
        $sort = 'ASC';
        $param = $request->query->all();
        
        if(!isset($param['page'])){
            $param['page'] = 0;
        }

        if($param['page'] > 1){
            $offset = $this->limit * $param['page'];
        }

        if(isset($param['sort'])){
            if($param['sort'] == 1){
                $sort = 'DESC';
            }
        }

        if(isset($param['filter']) && $param['filter'] == 1){
            foreach ($q_params as $name) {
                if(isset($param[$name])){
                    $query_params[$name] = $param[$name];
                }
            }
        }

        $query_total = $mosqueRepository->findBy(
            $query_params, 
            array('id' => $sort)
        );

        $query_result = $mosqueRepository->findBy(
            $query_params, 
            array('id' => $sort), 
            $this->limit, 
            $offset
        );

        $query_result = $mosqueRepository->findBy(
            $query_params, 
            array('id' => $sort), 
            $this->limit, 
            $offset
        );

        if(!empty($query_result)){

            $query_total = $this->serializer->normalize($query_total, 'array');
            $query_result = $this->serializer->normalize($query_result, 'array');
            


            foreach ($query_result as $key => $value) {
                $this->result['data'][] = $this->FormResponseArray($value, $this->traits, $value['id']);
            }
            
            $this->result['meta']['pagination']['limit'] = $this->limit;
            $this->result['meta']['pagination']['page'] = $param['page'];
            $this->result['meta']['pagination']['total'] = ceil(count($query_total) / $this->limit);
            $this->result['meta']['sort']['by'] = 'id';
            $this->result['meta']['sort']['order'] = 'ASC';
            $this->result['isSuccess'] = true;
        }else{
            $this->result['isSuccess'] = true;
        }


        return new JsonResponse($this->result, 200);
    }
}
