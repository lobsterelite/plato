<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\MosqueRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MosqueRepository::class)]
#[ApiResource]
class Mosque
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $background_color;

    #[ORM\Column(type: 'string', length: 255)]
    private $rarity;

    #[ORM\Column(type: 'integer')]
    private $rarityRank;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $attributes;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $signs;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $mosques;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $weather;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getBackgroundColor(): ?string
    {
        return $this->background_color;
    }


    public function getRarity(): ?string
    {
        return $this->rarity;
    }


    public function getRarityRank(): ?int
    {
        return $this->rarityRank;
    }

    public function getAttributes(): ?string
    {
        return $this->attributes;
    }


    public function getSigns(): ?string
    {
        return $this->signs;
    }


    public function getMosques(): ?string
    {
        return $this->mosques;
    }


    public function getWeather(): ?string
    {
        return $this->weather;
    }
    
}
